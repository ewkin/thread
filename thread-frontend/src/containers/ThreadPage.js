import React, {useEffect} from 'react';
import OneThread from "../components/OneThread/OneThread";
import {Backdrop, CircularProgress, Fade, Modal} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ThreadForm from "../components/ThreadForm/ThreadForm";
import {useDispatch, useSelector} from "react-redux";
import {createThread, fetchThread, showModal} from "../store/actions/threadActions";
import Grid from "@material-ui/core/Grid";


const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));


const ThreadPage = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const open = useSelector(state => state.showModal);
    const threads = useSelector(state => state.thread);
    const loading = useSelector(state => state.threadLoading);

    useEffect(() => {
        dispatch(fetchThread());
    }, [dispatch]);

    const onFormSubmit = async threadData => {
        await dispatch(createThread(threadData));
    };


    const handleClose = () => {
        dispatch(showModal(false));
    };


    return (
        <>
            {loading ? (
                <Grid container justify='center' alignContent="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>) : threads.map(thread => (
                <OneThread
                    key={thread.id}
                    id={thread.id}
                    name={thread.name}
                    text={thread.text}
                    image={thread.image}
                />
            ))}
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <ThreadForm onSubmit={onFormSubmit}/>
                    </div>
                </Fade>
            </Modal>
        </>
    )
        ;
};

export default ThreadPage;