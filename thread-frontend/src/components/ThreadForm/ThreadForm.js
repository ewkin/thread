import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FileInput from "../../UI/Form/FileInput";

const ThreadForm = ({onSubmit}) => {
    const [state, setState] = useState({
        name: '',
        text: '',
        image: ''
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => ({
            ...prevState, [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.files[0];
        setState(prevState => ({
            ...prevState, [name]: value
        }));
    };


    return (
        <form onSubmit={submitFormHandler}>
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id="name"
                        label="Name"
                        name="name"
                        value={state.title}
                        onChange={inputChangeHandler}/>
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        multiline
                        rows={3}
                        required
                        variant="outlined"
                        id="text"
                        label="Text"
                        name="text"
                        value={state.description}
                        onChange={inputChangeHandler}/>
                </Grid>
                <Grid item xs>
                    <FileInput
                        name='image'
                        label='Image'
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">
                        Create
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default ThreadForm;