import React from 'react';
import {Card, CardContent, CardMedia} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    root: {
        maxWidth: '100%',
    },
    media: {
        width: '40%',
        height: 100,
        backgroundSize: "contain"

    },
});


const OneThread = ({name, text, image}) => {
    const classes = useStyles();

    let userName = 'Anonymous';
    if (name) {
        userName = name;
    }

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {userName}
                </Typography>
            </CardContent>
            {image ? (<CardMedia
                className={classes.media}
                image={apiURL + '/uploads/' + image}
            />) : null}
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {text}
                </Typography>
            </CardContent>
        </Card>
    );
};

export default OneThread;