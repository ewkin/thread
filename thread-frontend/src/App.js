import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolBar from "./UI/AppToolBar/AppToolBar";
import Container from "@material-ui/core/Container";
import ThreadPage from "./containers/ThreadPage";

const App = () => (
    <>
        <CssBaseline/>
        <header><AppToolBar/></header>
        <main>
            <Container maxWidth="xl">
                <ThreadPage/>

            </Container>
        </main>
    </>
);

export default App;
