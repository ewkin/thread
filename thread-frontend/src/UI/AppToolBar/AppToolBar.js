import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {showModal} from "../../store/actions/threadActions";

const useStyles = makeStyles(theme => ({
    mainLink: {
        color: 'inherit',
        marginLeft: theme.spacing(10),
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit'
        },
    },
    staticToolbar: {
        marginBottom: theme.spacing(2)
    },
    appBar: {
        backgroundColor: "black",
    }
}));

const AppToolBar = () => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const openModal = () => {
        dispatch(showModal(true));
    }

    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6">
                        <Link to="/" className={classes.mainLink}>9GAG</Link>
                        <Link to='#' onClick={openModal} className={classes.mainLink}>Start thread</Link>
                    </Typography>
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar}/>
        </>
    );
};

export default AppToolBar;