import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_THREAD_REQUEST = 'FETCH_THREAD_REQUEST';
export const FETCH_THREAD_SUCCESS = 'FETCH_THREAD_SUCCESS';
export const FETCH_THREAD_FAILURE = 'FETCH_THREAD_FAILURE';
export const SHOW_MODAL = 'SHOW_MODAL';

export const CREATE_THREAD_SUCCESS = 'CREATE_THREAD_SUCCESS';

export const fetchThreadRequest = () => ({type: FETCH_THREAD_REQUEST});
export const fetchThreadSuccess = thread => ({type: FETCH_THREAD_SUCCESS, thread});
export const fetchThreadFailure = () => ({type: FETCH_THREAD_FAILURE});

export const showModal = action => ({type: SHOW_MODAL, action});

export const createThreadSuccess = () => ({type: CREATE_THREAD_SUCCESS});

export const fetchThread = () => {
    return async dispatch => {
        try {
            dispatch(fetchThreadRequest());
            const response = await axiosApi.get('/thread');
            dispatch(fetchThreadSuccess(response.data));
        } catch (e) {
            dispatch(fetchThreadFailure());
            NotificationManager.error('Could not fetch threads')
        }
    }
};

export const createThread = treadData => {
    return async dispatch => {
        try{
            await axiosApi.post('/thread', treadData);
            dispatch(fetchThread());
            dispatch(showModal(false));
            dispatch(createThreadSuccess());
        } catch (e) {
            NotificationManager.error('Could not post your thread');
            dispatch(fetchThreadFailure());
        }

    };
};