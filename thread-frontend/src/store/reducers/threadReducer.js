import {FETCH_THREAD_FAILURE, FETCH_THREAD_REQUEST, FETCH_THREAD_SUCCESS, SHOW_MODAL} from "../actions/threadActions";

const initialState = {
    thread: [],
    threadLoading: false,
    showModal: false,
};

const threadReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_THREAD_REQUEST:
            return {...state, threadLoading: true};
        case FETCH_THREAD_SUCCESS:
            return {...state, thread: action.thread, threadLoading: false};
        case FETCH_THREAD_FAILURE:
            return {...state, threadLoading: false};
        case SHOW_MODAL:
            return {...state, showModal: action.action};
        default:
            return state;
    }
};

export default threadReducer;