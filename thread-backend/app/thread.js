const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fileDb');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    const threads = await fileDb.getItems();
    res.send(threads);
});


router.post('/', upload.single('image'), async (req, res) => {
    const threads = req.body;
    if (req.file) {
        threads.image = req.file.filename;
    }
    if (threads.text === '') {
        return res.status(400).send({"error": 'Text must be present in the request'});
    }

    await fileDb.addItem(threads);
    res.send(threads);
});

module.exports = router;